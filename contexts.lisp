;;;; -*- Mode: Lisp -*-

;;;; contexts.lisp
;;;; Contexts to be used with the "WITH" macro.

(in-package "CTXTS")

(defclass context ()
  ()
  (:documentation "The Context Class.

A class that can be used in conjunction with the WITH macro.
This class is the top of the hierarchy of \"context\" classes.  It
shuld not be directly instantiated."))


(defgeneric is-context (x)
  (:method ((c context)) t)
  (:method ((c t)) nil)
  (:documentation
   "Returns T if the argument X is a CONTEXT; NIL otherwise."))


(defun context-p (x)
  "Returns T if the argument X is a CONTEXT; NIL otherwise."
  (is-context x))


;;;; end of file -- with-cl.lisp
