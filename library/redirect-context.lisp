;;;; -*- Mode: Lisp -*-

;;;; redirect-context.lisp
;;;; Another simple example from Python's contextlib.
;;;;
;;;; See file COPYING for copyright and licensing information.


(in-package "CTXTS")


(defclass redirect-context (context)
  ((stream :type stream
           :initarg :stream
           :reader redirection)
   (saved-stream :type stream
                 :accessor %%saved-stream)
   )

  (:documentation "The Redirect Context Class.

This is the context superclass for redirecting I/O.  The library
creates instances of this class subclasses.


See Also:

REDIRECT-OUTPUT-CONTEXT, REDIRECT-INPUT-CONTEXT,
REDIRECT-STANDARD-OUTPUT-CONTEXT, REDIRECT-ERROR-OUTPUT-CONTEXT,
ENTER, HANDLE, EXIT, WITH
")
  )


(defclass redirect-output-context (redirect-context)
  ())


(defclass redirect-input-context (redirect-context)
  ())


(defclass redirect-standard-output-context (redirect-output-context)
  ())


(defclass redirect-error-output-context (redirect-output-context)
  ())


(defgeneric is-redirect-context (x)
  (:method ((x redirect-context)) t)
  (:method ((x t)) nil)
  (:documentation "Returns T if the argument X is a REDIRECT-CONTEXT.")
  )


(defun redirect-context-p (x)
  "Returns T if the argument X is a REDIRECT-CONTEXT.

Notes:

This is a synonym of IS-REDIRECT-CONTEXT."
  (is-redirect-context x))


;;; Constructors.

(defmethod initialize-instance :before ((x redirect-output-context)
                                        &key (stream *standard-output*))
  (assert (streamp stream))
  (assert (output-stream-p stream)))


(defmethod initialize-instance :before ((x redirect-input-context)
                                        &key (stream *standard-input*))
  (assert (streamp stream))
  (assert (input-stream-p stream)))


(defun standard-output-redirection (stream)
  (make-instance 'redirect-standard-output-context :stream stream))


(defun error-output-redirection (stream)
  (make-instance 'redirect-error-output-context :stream stream))


(defun standard-input-redirection (stream)
  (make-instance 'redirect-input-context :stream stream))



;;;; ENTER/HANDLE/EXIT protocol.

(defmethod enter :after ((rsoc redirect-standard-output-context) &key)
  (setf (%%saved-stream rsoc) *standard-output*
        *standard-output* (redirection rsoc)))


(defmethod enter :after ((reoc redirect-error-output-context) &key)
  (setf (%%saved-stream reoc) *error-output*
        *error-output* (redirection reoc)))


(defmethod enter :after ((ric redirect-input-context) &key)
  (setf (%%saved-stream ric) *standard-input*
        *standard-input* (redirection ric)))


(defmethod handle ((rc redirect-context) (e error) &key)
  (call-next-method))


(defmethod exit :after ((rsoc redirect-standard-output-context) &key)
  (setf *standard-output* (%%saved-stream rsoc)))


(defmethod exit :after ((reoc redirect-error-output-context) &key)
  (setf *error-output* (%%saved-stream reoc)))


(defmethod exit :after ((ric redirect-input-context) &key)
  (setf *standard-input* (%%saved-stream ric)))


;;;; end of file -- redirect-context.lisp
