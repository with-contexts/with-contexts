;;;; -*- Mode: Lisp -*-

;;;; with-open-file.lisp
;;;; The WITH macro working as WITH-OPEN-FILE.
;;;;
;;;; Note that this implementation is "direct".  An "indirect" one
;;;; would wrap the stream in a NULL-CONTEXT; and this is what Python
;;;; appears to do with its decorator trick.
;;;; In Common Lisp we could to it either way; but the "direct" way
;;;; seems more natural and straightforward (and ... Common Lispy).

(in-package "CTXTS")


;;; enter

(defmethod enter ((context-item stream) &key)
  (if (open-stream-p context-item)
      context-item
      (error "Stream ~S is not open." context-item) ; This should be a
                                                    ; "more intelligent" error.
      ))


;;; handle
;;; No HANDLE really needed.

(defmethod handle ((context-item stream) (e error) &key)
  (call-next-method))


;;; exit

(defmethod exit ((context-item stream) &key)
  (when (open-stream-p context-item)
    (close context-item)))


;;;; end of file -- with-open-file.lisp
