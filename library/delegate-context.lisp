;;;; -*- Mode: Lisp -*-

;;;; delegate-context.lisp
;;;; Special context.
;;;;
;;;; See file COPYING for copyright and licensing information.


(in-package "CTXTS")

(defclass delegate-context (context)
  ((enter-result :initarg :enter-result
                 :reader enter-result))
                 
  (:default-initargs :enter-result nil)

  (:documentation "The Delegate Context Class.

A context that just wraps a value to which it delegates its behavior.

Notes:

This context it necessary in Common Lisp as the ENTER/HANDLE/EXIT
protocol can be implemented indipendently of 'being a context'.

See Also:

ENTER, HANDLE, EXIT, WITH, DELEGATE-CONTEXT (Function)
")
  )


(defgeneric is-delegate-context (x)
  (:method ((x delegate-context)) t)
  (:method ((x t)) nil))


(defun delegate-context-p (x) (is-delegate-context x))


;;; Constructors.

(defun delegate (&optional enter-result)
  "Creates a DELEGATE-CONTEXT stashing ENTER-RESULT for ENTER.

A subsequent call to ENTER on the instance will extract ENTER-RESULT and return it.


See Also:

ENTER, HANDLE, EXIT, WITH, DELEGATE-CONTEXT (Class)
"
  (make-instance 'delegate-context :enter-result enter-result)
  )


;;;; ENTER/HANDLE/EXIT protocol.

(defmethod enter ((nc delegate-context) &key)
  (enter (enter-result nc)))


(defmethod handle ((nc delegate-context) (e error) &key)
  (call-next-method))


(defmethod exit ((nc delegate-context) &key)
  (if (slot-boundp nc 'enter-result)
      (exit (enter-result nc))
      (call-next-method)))


;;;; end of file -- delegate-context.lisp
