;;;; -*- Mode: Lisp -*-

;;;; exit-stack-context.lisp
;;;; Another simple example from Python's contextlib.
;;;;
;;;; See file COPYING for copyright and licensing information.


(in-package "CTXTS")


(defclass exit-stack-context (context)
  ((callback-stack :initform ()
                   :type list ; A stack.
                   :reader callback-stack
                   :writer set-callback-stack ; Unexported. See POP-ALL below.
                   ))
  (:documentation "The Exit Stack Class.

A class that serves to collect and unwind code (callbacks) that are
collected within a WITH macro body.  The methods of this class provide
more control over what happens with errors that may happen within
ENTER calls in non-straightforward contexts.

Notes:

This class provides much of the functionalities displayed in the Python
examples.
")
  )


(defgeneric is-exit-stack-context (x)
  (:method ((x exit-stack-context)) t)
  (:method ((x t)) nil)
  (:documentation
   "Returns T when the argument X is and EXIT-CONTEXT; NIL otherwise."))


(defun exit-stack-context-p (x)
  "Returns T when the argument X is and EXIT-CONTEXT; NIL otherwise."
  (is-exit-stack-context x))



(defclass error-context (context) ; Maybe it should be a condition.
  ((ctx :initform ()
        :reader error-context-item
        :initarg :item
        )

   (cnd :initform ()
        :reader error-context-condition
        :initarg :error
        :initarg :condition
        ))
  )


(defgeneric is-error-context (x)
  (:method ((x error-context)) t)
  (:method ((x t)) nil))


(defun error-context-p (x) (is-error-context x))
   

;;; Constructors.

(defun exit-stack ()
  "Constructor for an EXIT-STACK-CONTEXT."
  (make-instance 'exit-stack-context))


(defun error-context (item error)
  "Constructor for an EXIT-STACK-CONTEXT."
  (make-instance 'error-context :item item :error error))



;;; Exit Stack methods as hinted by the Python - sketchy - documentation.
;;; enter-context

(defgeneric enter-context (exit-stack context-manager)
  (:method ((es exit-stack-context) (cm context))
   (with-slots (callback-stack) es
     (handler-case
         (prog1 (enter cm)
           (push cm callback-stack))           
       (error (e) (push (error-context cm e) callback-stack))
       )))
  (:documentation
   "Calls ENTER on a CONTEXT-MANAGER and pushes it on the EXIT-STACK callback stack.

If an error occurs while ENTERing the CONTEXT-MANAGER, then an
ERROR-CONTEXT is pushed on the EXIT-CONTEXT callback stack."))


(defgeneric push-context (exit-stack context-manager)
  (:method ((es exit-stack-context) (cm context))
   ;; This could be a :before method for ENTER-CONTEX.
   (with-slots (callback-stack) es
     (push cm callback-stack))
   cm)
  (:documentation
   "Pushes a CONTEXT-MANAGER on the callback stack of EXIT-MANAGER."))


(defgeneric callback (exit-stack function &rest args)
  (:method ((es exit-stack-context) (f function) &rest args)
   (with-slots (callback-stack) es
     (push (cons f args) callback-stack))
   f)
  (:documentation "Adds a FUNCTION and its ARGS to an EXIT-STACK."))


(defgeneric pop-all (exit-stack-context)
  (:method ((es exit-stack-context))
   (let ((nes (exit-stack)))
     (set-callback-stack (callback-stack es) nes)
     (set-callback-stack () es)

     ;; Note that this zeroing the callback stack is implied by the
     ;; "transfer" in the Python's documentation.
     ;; Plus the 'files' example would not work if it weren't.

     nes))
  (:documentation
   "Creates a new EXIT-STACK-CONTEXT and transfers the callbacks to it.

The callbacks of the current exit stack are popped from it and moved
to the new one."
   ))


(defgeneric unwind (exit-stack)
  ;; This is the CLOSE method in Python contextlib library.
  ;; Note that the semantics is different as we also have the HANDLE
  ;; method handy.

  (:method ((es exit-stack-context))
   (loop with cs = (copy-list (callback-stack es))
         while cs do
         (let ((item (pop cs)))
           (typecase item
             (context (exit item))
             (list    (apply (first item) (rest item)))
             ;; (t (error "Should this be an error?"))
             )))
   )
  (:documentation
   "The callback stack of the EXIT-STACK are popped and called.

Notes:

This is the method 'close()' in Python.  The name 'UNWIND' was chosen
to be more 'lispy' and to avoid conclictsa with CL:CLOSE.")
  )
   

;;;; ENTER/HANDLE/EXIT protocol.

(defmethod enter ((es exit-stack-context) &key)
  es)


(defmethod handle ((es exit-stack-context) (e error) &key)
  ;; The question is what to do with the call stack at this point.
  ;; If the top of the stack is an ERROR-CONTEXT (note: better call it
  ;; EXIT-STACK-ERROR-CONTEXT), then this is
  ;; something signaled by the last ENTER-CONTEXT call the ENTER.
  ;; Therefore we HANDLE the error on the wrapped, error generatng
  ;; context.

  (with-slots (callback-stack) es
    (let ((top (first callback-stack)))
      (if (typep top 'error-context)

          ;; Could also ASSERT equality of E and
          ;; (ERROR-CONTEXT-CONDITION TOP)

          (progn
            (pop callback-stack)
            (handle (error-context-item top)
                    (error-context-condition top)))
          (error e)))))


(defmethod exit ((es exit-stack-context) &key)
  (unwind es))


;;;; Example from Python doc.
;;;; This should be pretty much equivalent.

#| New syntax
(with stack = (exit-stack) do
  (let* ((files (mapcar (lambda (fname)
                          (enter-context stack (open fname))) ; Need to wrap OPEN.
                        *filenames*)))

    ;; Hold on to the new exit stack (not the method pointer as in the
    ;; Python example), but don't call its UNWIND method

    (setf *close-files* (pop-all stack))

    ;; If opening any file fails, all previously opened files will be
    ;; closed automatically. If all files are opened successfully,
    ;; they will remain open even after the with statement ends.
    ;;
    ;;    (unwind *close-files*)
    ;;
    ;; can then be invoked explicitly to close them all.

    ;; ...
    )
|#
    


;;;; end of file -- exit-stack-context.lisp
